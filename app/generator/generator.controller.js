/**
 * Created by jswensen on 3/8/14.
 */
angular.module('generator.controller', []);

angular.module('generator.controller').controller('GeneratorController',
['$scope', '$log', function($scope, $log) {
    $scope.model = {};

    var hoursPerDay = 8;

    $scope.selectTab = function(tabName) {
        $('ul.nav-tabs > li').removeClass('active');
        $('div.tab-pane').removeClass('active').hide();
        $('#'+tabName+'-tab').addClass('active');
        $('#'+tabName+'-tabPane').show();
    }

    $scope.generateSchedule = function() {
        var timeSlotsPerDay = hoursPerDay / $scope.match.matchLength;
        $log.debug("TimeSlotsPerDay = " + timeSlotsPerDay);

        // total time slots available
        var totalTimeSlots =
            ($scope.calendar.days * timeSlotsPerDay) * $scope.locations.number;
        $log.debug("TotalTimeSlots = " + totalTimeSlots);

        var totalMatches = $scope.teams.number


    }
}]);