/**
 * Created by jswensen on 3/8/14.
 */
angular.module('navbar.directive', []);

angular.module('navbar.directive').directive('navbar', [function() {
    return {
        restrict: 'E',
        templateUrl: '/ng-halftime/app/navbar/navbar.directive.tpl.html',
        scope: {},
        controller: 'NavbarController'
    };
}]);

angular.module('navbar.directive').controller('NavbarController', ['$scope', function($scope) {

}]);