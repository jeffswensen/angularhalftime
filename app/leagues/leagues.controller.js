/**
 * Created by jswensen on 3/8/14.
 */
angular.module('leagues.controller', []);

angular.module('leagues.controller').controller('LeaguesController', ['$scope', function($scope) {


    $scope.sports= [
        { key: 'baseball', longName: 'Baseball'},
        { key: 'volleyball', longName: 'Volleyball'},
        { key: 'soccer', longName: 'Soccer'},
        { key: 'fieldhockey', longName: 'Field Hockey'},
        { key: 'football', longName: 'Football'}
    ];

    $scope.sportLongName = function(key) {
        var longName = '';
        for (var i = 0; i < $scope.sports.length; i++) {
            if (key === $scope.sports[i].key) {
                longName = $scope.sports[i].longName;
                break;
            }
        }
        return longName;
    };

    $scope.leagues = [
        {
            id: 1,
            name: 'Little League',
            sport: 'baseball'
        },
        {
            id: 2,
            name: 'Intramural Volleyball',
            sport: 'volleyball'
        },
        {
            id: 3,
            name: '10-12 Boys Traveling Soccer',
            sport: 'soccer'
        },
        {
            id: 4,
            name: '12-14 Girls Field Hockey',
            sport: 'fieldhockey'
        },
        {
            id: 5,
            name: 'PeeWee Football',
            sport: 'football'
        }
    ];
}]);