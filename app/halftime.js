/**
 * Created by jswensen on 3/8/14.
 */
angular.module('halftime', [
    'ngRoute',
    'navbar',
    'home',
    'leagues',
    'generator'
]);

angular.module('halftime').config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/ng-halftime/app/home/home.tpl.html',
            controller: 'HomeController'
        })
        .when('/leagues', {
            templateUrl: '/ng-halftime/app/leagues/leagues.tpl.html',
            controller: 'LeaguesController'
        })
        .when('/generator', {
            templateUrl: '/ng-halftime/app/generator/generator.tpl.html',
            controller: 'GeneratorController'
        })
        .otherwise({redirectTo: '/'})
}]);